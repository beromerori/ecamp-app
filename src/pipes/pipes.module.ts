import { NgModule } from '@angular/core';

// My pipes
import { FormatDatePipe } from './format-date/format-date';

@NgModule({
	declarations: [
		FormatDatePipe
	],
	imports: [],
	exports: [
		FormatDatePipe
	]
})
export class PipesModule { }