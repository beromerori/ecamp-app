import { Pipe, PipeTransform } from '@angular/core';

// Moment
import * as moment from 'moment';

/**
 * Generated class for the FormatDatePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'formatDate',
})
export class FormatDatePipe implements PipeTransform {

  constructor() {
    moment.locale('es');
  }

  transform(value: any, args?: any): any {
    return moment(value).format('l');
  }
}