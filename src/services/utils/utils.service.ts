import { Injectable } from '@angular/core';

// CONST
import { CONST } from '../../app/app.constants';

/*
  Generated class for the UtilsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsService {

  constructor() { }

  // Modalities

  public getCheckedModalities(modalitiesIds: number[]) {

    let modalities = JSON.parse(JSON.stringify(CONST.MODALITIES));

    modalities = modalities.map(modality => {

      const index = modalitiesIds.findIndex(modalityId => modalityId === modality.id);
      if (index > -1) modality.checked = true;

      return modality;
    })

    return modalities.filter(modality => modality.checked);
  }
}