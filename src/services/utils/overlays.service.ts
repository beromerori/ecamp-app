import { Injectable } from '@angular/core';
import { AlertController, Loading, LoadingController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

// Rxjs
import { forkJoin, from, Observable, of, throwError } from 'rxjs';
import { catchError, delayWhen, mapTo, shareReplay, startWith, switchMap, delay } from 'rxjs/operators';

@Injectable()
export class OverlaysService {

  private loading: Loading;
  //private alert: Alert;

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private translateService: TranslateService) { }

  public requestWithLoaderAndError(request: () => Observable<any>, msg?: string) {

    const obs = of({}).pipe(
      this.presentLoader(),
      switchMap(() => request().pipe(this.dismissLoaderAndPresentError())),
      this.dismissLoader(),
      shareReplay()
    );

    obs.subscribe(
      (success) => {

        if (msg) {

          const toast = this.toastCtrl.create({
            message: this.translateService.instant(msg),
            duration: 3000,
            position: 'top'
          });

          toast.present();
        }
      },
      (error) => { }
    );

    return obs;
  }

  public buildLoader(obs: Observable<any>) {

    return obs.pipe(
      delay(200),
      mapTo(false),
      startWith(true),
      this.dismissLoaderAndPresentError(),
      catchError((error) => of(false)),
      shareReplay()
    );
  }

  public dismissLoaderAndPresentError() {

    return catchError(error =>
      forkJoin(
        from(this.hideLoader()),
        from(this.presentErrorAlert(error))
      ).pipe(switchMap(() => throwError({ error: error.error.message })))
    );
  }

  // Loading

  public presentLoader() {
    return delayWhen(() => from(this.showLoader()));
  }

  private async showLoader() {

    if (!this.loading) {

      this.loading = this.loadingCtrl.create({
        content: this.translateService.instant('LOADING')
      });

      await this.loading.present();
    }
  }

  public dismissLoader() {
    return delayWhen(() => from(this.hideLoader()));
  }

  private async hideLoader() {

    if (this.loading) {
      await this.loading.dismiss();
      this.loading = null;
    }
  }

  // Alert

  public async presentErrorAlert(error: { error: { status: number, message: string } }) {

    if (error.error.status === 502) return;
    //if (!error.error.status) error.error.message = 'SERVER.ERRORS.NOT-CONNECTION';

    const alert = this.alertCtrl.create({
      title: this.translateService.instant('ALERT.INFO'),
      subTitle: this.translateService.instant(error.error.message),
      buttons: [this.translateService.instant('ALERT.ACCEPT')]
    });

    await alert.present();
  }
}