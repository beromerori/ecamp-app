import { Injectable } from '@angular/core';

// My models
import { Bill, Camp, Child, Instructor, Message, Parent, Programming, User } from '../../models';

@Injectable()
export class BaseEntityService {

  constructor() { }

  private modelType = {
    'bill': (data) => new Bill(data),
    'camp': (data) => new Camp(data),
    'user': (data) => new User(data),
    'admin': (data) => new User(data),
    'instructor': (data) => new Instructor(data),
    'parent': (data) => new Parent(data),
    'child': (data) => new Child(data),
    'message': (data) => new Message(data),
    'programming': (data) => new Programming(data)
  }

  public createModel(type: string, data: any) {
    const modelTypeHandler = this.modelType[type];
    return modelTypeHandler(data);
  }

  public createList(type: string, entities: any[]) {
    return entities.map((entity) => this.createModel(type, entity));
  }

  public updateEntity(type: string, currentEntities: any[], newEntity: any) {

    const index = currentEntities.findIndex(entity => entity._id === newEntity._id);

    if (index > -1) {
      currentEntities[index] = this.createModel(type, newEntity);
    }
    else currentEntities.push(this.createModel(type, newEntity));

    return currentEntities;
  }

  public removeEntity(currentEntities: any[], deletedEntityId: any) {

    const index = currentEntities.findIndex(entity => entity._id === deletedEntityId);

    if (index > -1) {
      currentEntities.splice(index, 1);
    }

    return currentEntities;
  }

  public updateList(type: string, currentEntities: any[], newEntities: any[]) {

    newEntities.forEach(newEntity => {

      const index = currentEntities.findIndex(entity => entity._id === newEntity._id);

      if (index > -1) {
        currentEntities[index] = this.createModel(type, newEntity);
      }
      else currentEntities.push(this.createModel(type, newEntity));

    });

    return currentEntities;
  }
}