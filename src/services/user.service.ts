import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { find, map, pluck, filter } from 'rxjs/operators';

// My models
import { Child, User, UserCredentials } from '../models';

// My services
import { BaseEntityService } from './utils/base-entity.service';
import { BaseService } from './base-service';
import { ProfileService } from './profile.service';

export interface State {
  users?: User[]
}

@Injectable()
export class UserService {

  private initialState = {
    users: []
  }

  private usersSubject$ = new BehaviorSubject<State>(this.initialState);
  public users$ = this.usersSubject$.asObservable().pipe(pluck('users'));

  constructor(
    private profileService: ProfileService,
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.usersSubject$.getValue();
  }

  get currentUsers() {
    return this.usersSubject$.getValue().users;
  }

  get currentChilds() {
    return this.usersSubject$.getValue().users.filter(user => !user.role);
  }

  public getChilds() {

    const id: string = this.profileService.currentProfile._id;

    const request = this.baseService.get(`users/${id}/childs`);

    request.subscribe(
      (childs: Child[]) => this.usersSubject$.next({ users: this.baseEntityService.createList('child', childs) }),
      (error) => { }
    );

    return request;
  }

  public getInstructor(camp_id: string) {

    const request = this.baseService.get('users', { camp_id });

    request.subscribe(
      (users: User[]) => this.usersSubject$.next({ users: this.baseEntityService.updateList('user', [...this.currentUsers], users) }),
      (error) => { }
    );

    return request;
  }

  public updateUserPassword(body: UserCredentials) {

    const request = this.baseService.put('users', body);

    request.subscribe(
      (success) => { },
      (error) => { }
    );

    return request;
  }

  // SELECTS

  public selectById(id: string) {

    return this.users$.pipe(
      map((users: User[]) => User.findById(users, id)),
      find(e => Boolean(e))
    );
  }

  public filterChilds() {

    const parent_id: string = this.profileService.currentProfile._id;

    return this.users$.pipe(
      map((users: User[]) => User.filterChilds(users, parent_id)),
      filter(e => Boolean(e))
    );
  }

  public filterInstructors() {

    return this.users$.pipe(
      map((users: User[]) => User.filterInstructors(users)),
      filter(e => Boolean(e))
    );
  }
}