import { Injectable } from '@angular/core';

// My models
import { MediaFile, MediaFileDeleted } from '../models';

// My services
import { BaseService } from './base-service';

@Injectable()
export class MediaService {

  constructor(private baseService: BaseService) { }

  /** ---------- POST ---------- */

  private urlTypes = {
    'camp': () => { return { entity: 'camp', url: 'camps/media/files' } },
    'user': () => { return { entity: 'user', url: 'users/media/files' } },
  }

  public uploadFile(type: string, code: string, base64: string): Promise<MediaFile> {

    const urlTypesHandler = this.urlTypes[type];
    const entity = urlTypesHandler().entity;
    const url = urlTypesHandler().url;

    const body = {
      type: entity,
      code: code,
      file: base64
    };

    return new Promise((resolve, reject) => {
      this.baseService.post(url, body).subscribe(
        (fileUploded: MediaFile) => resolve(fileUploded),
        (error) => reject(error)
      );
    });
  }

  public deleteFile(type: string, file: MediaFile) {

    const urlTypesHandler = this.urlTypes[type];
    const entity = urlTypesHandler().entity;
    const url = `${urlTypesHandler().url}/${file.public_id}`;

    const params = {
      type: entity
    };

    return new Promise((resolve, reject) => {
      this.baseService.delete(url, params).subscribe(
        (fileDeleted: MediaFileDeleted) => resolve(fileDeleted),
        (error) => reject(error)
      );
    });
  }

  public deleteAllFiles(type: string, code: string) {

    const urlTypesHandler = this.urlTypes[type];
    const entity = urlTypesHandler().entity;
    const url = urlTypesHandler().url;

    const params = {
      type: entity,
      folder: code
    }

    return new Promise((resolve, reject) => {
      this.baseService.delete(url, params).subscribe(
        (success) => resolve(),
        (error) => reject(error)
      );
    });
  }
}