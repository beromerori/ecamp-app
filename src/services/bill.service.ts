import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { Bill } from '../models';

// My services
import { BaseEntityService } from './utils/base-entity.service';
import { BaseService } from './base-service';
import { ProfileService } from './profile.service';

export interface State {
  bills?: Bill[]
}

@Injectable()
export class BillService {

  private initialState = {
    bills: []
  }

  private billsSubject$ = new BehaviorSubject<State>(this.initialState);
  public bills$ = this.billsSubject$.asObservable().pipe(pluck('bills'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService,
    private profileService: ProfileService) { }

  get state() {
    return this.billsSubject$.getValue();
  }

  get currentBills() {
    return this.billsSubject$.getValue().bills;
  }

  public getBills() {

    const parentDni: string = this.profileService.currentProfile.dni;

    const request = this.baseService.get(`bills`, { parentDni });

    request.subscribe(
      (bills: Bill[]) => {
        this.billsSubject$.next({ bills: this.baseEntityService.createList('bill', bills) });
      },
      (error) => { }
    );

    return request;
  }
}