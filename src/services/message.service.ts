import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { Message } from '../models';

// My services
import { BaseEntityService } from './utils/base-entity.service';
import { BaseService } from './base-service';

export interface State {
  messages?: Message[]
}

@Injectable()
export class MessageService {

  private initialState = {
    messages: []
  }

  private messagesSubject$ = new BehaviorSubject<State>(this.initialState);
  public messages$ = this.messagesSubject$.asObservable().pipe(pluck('messages'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.messagesSubject$.getValue();
  }

  get currentMessages() {
    return this.messagesSubject$.getValue().messages;
  }

  public getMessages(parent_id: string) {

    const request = this.baseService.get('messages', { parent_id });

    request.subscribe(
      (messages: Message[]) => this.messagesSubject$.next({ messages: this.baseEntityService.createList('message', messages) }),
      (error) => { }
    );

    return request;
  }
}