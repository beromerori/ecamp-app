import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { filter, map, pluck } from 'rxjs/operators';

// My models
import { Programming } from '../models';

// My services
import { BaseEntityService } from './utils/base-entity.service';
import { BaseService } from './base-service';

export interface State {
  programmings?: Programming[]
}

@Injectable()
export class ProgrammingService {

  private initialState = {
    programmings: []
  }

  private programmingsSubject$ = new BehaviorSubject<State>(this.initialState);
  public programmings$ = this.programmingsSubject$.asObservable().pipe(pluck('programmings'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.programmingsSubject$.getValue();
  }

  get currentProgrammings() {
    return this.programmingsSubject$.getValue().programmings;
  }

  public getProgrammings(camp_id) {

    const request = this.baseService.get('programmings', { camp_id });

    request.subscribe(
      (programmings: Programming[]) => this.programmingsSubject$.next({ programmings: this.baseEntityService.createList('programming', programmings) }),
      (error) => { }
    );

    return request;
  }

  public filterByCamp(camp_id: string) {

    return this.programmings$.pipe(
      map((programmings: Programming[]) => Programming.filterByCamp(programmings, camp_id)),
      filter(e => Boolean(e))
    );
  }
}