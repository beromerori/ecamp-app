import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { find, map, pluck } from 'rxjs/operators';

// My models
import { Camp } from '../models';

// My services
import { BaseEntityService } from './utils/base-entity.service';
import { BaseService } from './base-service';
import { UtilsService } from './utils/utils.service';

export interface State {
  camps?: Camp[]
}

@Injectable()
export class CampService {

  private initialState = {
    camps: []
  }

  private campsSubject$ = new BehaviorSubject<State>(this.initialState);
  public camps$ = this.campsSubject$.asObservable().pipe(pluck('camps'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService,
    private utilsService: UtilsService) { }

  get state() {
    return this.campsSubject$.getValue();
  }

  get currentCamps() {
    return this.campsSubject$.getValue().camps;
  }

  public getCamp(camp: Camp) {

    const request = this.baseService.get(`camps/${camp.camp_id}`);

    request.subscribe(
      (camp: Camp) => {
        camp.modalities = this.utilsService.getCheckedModalities(camp.modalities);
        this.campsSubject$.next({ camps: this.baseEntityService.updateEntity('camp', [...this.currentCamps], camp) });
      },
      (error) => { }
    );

    return request;
  }

  // SELECTS

  public selectById(id: string) {
    return this.camps$.pipe(
      map((camps: Camp[]) => Camp.findById(camps, id)),
      find(e => Boolean(e))
    );
  }
}