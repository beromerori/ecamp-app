import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { UserCredentials, UserLogged } from '../models';

// My services
import { BaseService } from './base-service';

export interface State {
  userLogged?: UserLogged
}

@Injectable()
export class AuthService {

  private initialState = {
    userLogged: {
      token: '',
      isLogged: false
    }
  };

  private userLoggedSubject$ = new BehaviorSubject<State>(this.initialState);
  public userLogged$ = this.userLoggedSubject$.asObservable().pipe(pluck('userLogged'));

  constructor(private baseService: BaseService) {

    const email: string = localStorage.getItem('email');
    const token: string = localStorage.getItem('token');

    if (email && token) {
      const userLogged = { token, isLogged: true };
      this.userLoggedSubject$.next({ userLogged });
    }
  }

  get state() {
    return this.userLoggedSubject$.getValue();
  }

  get currentUserLogged() {
    return this.userLoggedSubject$.getValue().userLogged;
  }

  public isUserLogged() {
    return Boolean(this.currentUserLogged.token);
  }

  public login(userCredentials: UserCredentials) {

    const request = this.baseService.post('login', userCredentials);

    request.subscribe(
      (userLogged: UserLogged) => {
        localStorage.setItem('token', userLogged.token);
        localStorage.setItem('email', userCredentials.email);
        userLogged.isLogged = true;
        this.userLoggedSubject$.next({ userLogged })
      },
      (error) => { }
    );

    return request;
  }

  public logout() {
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    this.userLoggedSubject$.next({ userLogged: { token: '', isLogged: false } });
  }
}