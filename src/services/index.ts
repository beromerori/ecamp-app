// Auth
import { AuthService } from './auth.service';
export { AuthService } from './auth.service';

// Base-service
import { BaseService } from './base-service';
export { BaseService } from './base-service';

// Base-entity-service
import { BaseEntityService } from './utils/base-entity.service';
export { BaseEntityService } from './utils/base-entity.service';

// Bill
import { BillService } from './bill.service';
export { BillService } from './bill.service';

// Camp
import { CampService } from './camp.service';
export { CampService } from './camp.service';

// Media
import { MediaService } from './media.service';
export { MediaService } from './media.service';

// Message
import { MessageService } from './message.service';
export { MessageService } from './message.service';

// Overlays
import { OverlaysService } from './utils/overlays.service';
export { OverlaysService } from './utils/overlays.service';

// Profile
import { ProfileService } from './profile.service';
export { ProfileService } from './profile.service';

// Programming
import { ProgrammingService } from './programming.service';
export { ProgrammingService } from './programming.service';

// User
import { UserService } from './user.service';
export { UserService } from './user.service';

// Utils
import { UtilsService } from './utils/utils.service';
export { UtilsService } from './utils/utils.service';

export const Services = [
    AuthService,
    BaseService,
    BaseEntityService,
    BillService,
    CampService,
    MediaService,
    MessageService,
    OverlaysService,
    ProfileService,
    ProgrammingService,
    UserService,
    UtilsService
];