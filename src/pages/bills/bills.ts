import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

// My components (barrel)
import { CampDetailModalComponent } from '../../components/modals';

// My models
import { Camp } from '../../models';

// My services (barrel)
import { BillService, OverlaysService } from '../../services';

/**
 * Generated class for the BillsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bills',
  templateUrl: 'bills.html',
})
export class BillsPage {

  public loading$;
  public bills$ = this.billsService.bills$;

  constructor(
    private modalCtrl: ModalController,
    private billsService: BillService,
    private overlaysService: OverlaysService) { }

  ionViewWillEnter() {
    this.getBills();
  }

  private getBills() {
    this.loading$ = this.overlaysService.buildLoader(this.billsService.getBills());
  }

  public openCampDetail(camp: Camp) {

    const campDetailModal = this.modalCtrl.create(CampDetailModalComponent, { camp });
    campDetailModal.present();
  }
}