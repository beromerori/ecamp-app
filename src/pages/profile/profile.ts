import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActionSheet, ActionSheetController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

// Plugins
import { Camera, CameraOptions } from '@ionic-native/camera';

// CONST
import { CONST } from '../../app/app.constants';

// My models
import { User } from '../../models';

// My services (barrel)
import { MediaService, OverlaysService, ProfileService } from '../../services';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public languages = CONST.LANGUAGES;

  public profileForm: FormGroup;

  public loading$;
  public profile$ = this.profileService.profile$;

  public passwordsInvalid: boolean = false;

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private translateService: TranslateService,
    private mediaService: MediaService,
    private overlaysService: OverlaysService,
    private profileService: ProfileService) {

    this.initForm(this.profileService.currentProfile);
  }

  private initForm(user: User) {

    this.profileForm = new FormGroup({
      //_id: new FormControl(user._id),
      avatar: new FormControl({ secure_url: user.avatar ? user.avatar.secure_url : '' }),
      name: new FormControl(user.name, Validators.required),
      surnames: new FormControl(user.surnames, Validators.required),
      email: new FormControl(user.email, [Validators.required, Validators.pattern(CONST.VALIDATIONS.EMAIL_REGEX)]),
      phone: new FormControl(user.phone, Validators.required),
      pass: new FormGroup({
        newPass: new FormControl('', Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)),
        repeatPass: new FormControl('', Validators.pattern(CONST.VALIDATIONS.PASS_REGEX))
      }, this.areEquals),
      language: new FormControl(localStorage.getItem('lang') || 'es')
    });

    this.newPass.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.repeatPass.value)
    );

    this.repeatPass.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.newPass.value)
    );
  }

  public get avatar() { return this.profileForm.get('avatar'); }
  public get name() { return this.profileForm.get('name'); }
  public get surnames() { return this.profileForm.get('surnames'); }
  public get email() { return this.profileForm.get('email'); }
  public get phone() { return this.profileForm.get('phone'); }
  public get newPass() { return this.profileForm.get('pass').get('newPass'); }
  public get repeatPass() { return this.profileForm.get('pass').get('repeatPass'); }

  private areEquals(form: any) {

    const fields: string[] = [];

    for (let control in form.controls) {
      fields.push(control);
    }

    return form.controls[`${fields[0]}`].value === form.controls[`${fields[1]}`].value ? null : { equal: { valid: false } };
  }

  public changeAvatar() {

    const cameraActions: ActionSheet = this.actionSheetCtrl.create({
      title: this.translateService.instant('PROFILE.PROFILE-PICTURE'),
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: this.translateService.instant('PROFILE.CAMERA'),
          role: 'destructive',
          handler: () => {
            this.addPhoto(true);
          }
        },
        {
          text: this.translateService.instant('PROFILE.GALLERY'),
          handler: () => {
            this.addPhoto(false);
          }
        }
      ]
    });
    cameraActions.present();
  }

  async addPhoto(useCamera: boolean) {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: useCamera ? 1 : 0,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };

    const photo = await this.camera.getPicture(options);
    if (photo) {
      this.avatar.value.secure_url = `data:image/jpeg;base64,${photo}`;
    }
  }

  public changeLanguage(language: string) {
    localStorage.setItem('lang', language);
    this.translateService.use(language);
  }

  public async save() {

    if (this.avatar.value.secure_url.includes('data:image/jpeg;base64')) {
      const avatar = await this.mediaService.uploadFile('user', this.email.value, this.avatar.value.secure_url);

      if (avatar) this.avatar.patchValue(avatar);
    }

    const profile: User = {
      ...this.profileService.currentProfile,
      avatar: this.avatar.value,
      name: this.name.value,
      surnames: this.surnames.value,
      email: this.email.value,
      phone: this.phone.value,
      pass: this.newPass.value
    }

    if (!this.newPass.value) delete profile.pass;

    this.overlaysService.requestWithLoaderAndError(() => this.profileService.updateProfile(profile), 'SERVER.MESSAGES.USER.UPDATED');
  }
}