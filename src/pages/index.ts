// Bills
import { BillsPage } from './bills/bills';
export { BillsPage } from './bills/bills';

// Camp-list
import { CampListPage } from './programmings/camp-list/camp-list';
export { CampListPage } from './programmings/camp-list/camp-list';

// Childs
import { ChildsPage } from './childs/childs';
export { ChildsPage } from './childs/childs';

// Home
import { HomePage } from './home/home';
export { HomePage } from './home/home';

// Login
import { LoginPage } from './login/login';
export { LoginPage } from './login/login';

// Messages
import { MessagesPage } from './messages/messages';
export { MessagesPage } from './messages/messages';

// Pre-home
import { PreHomePage } from './pre-home/pre-home';
export { PreHomePage } from './pre-home/pre-home';

// Profile
import { ProfilePage } from './profile/profile';
export { ProfilePage } from './profile/profile';

// Programmings
import { ProgrammingsPage } from './programmings/programmings';
export { ProgrammingsPage } from './programmings/programmings';

export const Pages: any = [
    BillsPage,
    CampListPage,
    ChildsPage,
    HomePage,
    LoginPage,
    MessagesPage,
    PreHomePage,
    ProfilePage,
    ProgrammingsPage
];