import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

// My models
import { Child } from '../../models';

// My components(barrel)
import { InstructorDetailModalComponent } from '../../components/modals';

// My services (barrel)
import { OverlaysService, UserService } from '../../services';

/**
 * Generated class for the ChildsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-childs',
  templateUrl: 'childs.html',
})
export class ChildsPage {

  public loading$;
  public childs$ = this.usersService.users$;

  constructor(
    private modalCtrl: ModalController,
    private overlaysService: OverlaysService,
    private usersService: UserService
  ) { }

  ionViewWillEnter() {
    this.getChilds();
  }

  private getChilds() {

    this.loading$ = this.overlaysService.buildLoader(this.usersService.getChilds());
    this.childs$ = this.usersService.filterChilds();
  }

  public openInstructorDetail(child: Child) {

    const instructorDetailModal = this.modalCtrl.create(InstructorDetailModalComponent, { camp_id: child.camp.camp_id });
    instructorDetailModal.present();
  }
}