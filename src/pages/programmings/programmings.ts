import { Component } from '@angular/core';
import { ModalController, NavParams } from 'ionic-angular';

// My components (barrel)
import { ProgrammingDetailModalComponent } from '../../components/modals';

// My models
import { Programming } from '../../models';

// My services (barrel)
import { OverlaysService, ProgrammingService } from '../../services';

/**
 * Generated class for the ProgrammingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-programmings',
  templateUrl: 'programmings.html',
})
export class ProgrammingsPage {

  private camp;
  public loading$;
  public programmings$ = this.programmingService.programmings$;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private overlaysService: OverlaysService,
    private programmingService: ProgrammingService) {

    this.camp = this.navParams.get('camp');
  }

  ionViewWillEnter() {
    this.getProgrammings();
  }

  private getProgrammings() {
    this.loading$ = this.overlaysService.buildLoader(this.programmingService.getProgrammings(this.camp.camp_id));
    this.programmings$ = this.programmingService.filterByCamp(this.camp.camp_id);
  }

  public openProgrammingDetail(programming: Programming) {

    const programmingDetailModal = this.modalCtrl.create(ProgrammingDetailModalComponent, { programming });
    programmingDetailModal.present();
  }
}