import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// My models
import { Camp } from '../../../models';

// My services (barrel)
import { UserService } from '../../../services';
import { ProgrammingsPage } from '../programmings';

/**
 * Generated class for the CampListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-camp-list',
  templateUrl: 'camp-list.html',
})
export class CampListPage {

  public childs$ = this.userService.filterChilds();

  constructor(
    private navCtrl: NavController,
    private userService: UserService) { }

  ionViewWillEnter() {
    this.childs$ = this.userService.filterChilds();
  }

  public goToCampProgramming(camp: Camp) {
    this.navCtrl.push(ProgrammingsPage, { camp });
  }
}