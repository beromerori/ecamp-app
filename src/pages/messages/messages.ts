import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

// My components (barrel)
import { MessageDetailModalComponent } from '../../components/modals';

// My models
import { Message } from '../../models';

// My services (barrel)
import { MessageService, OverlaysService, ProfileService } from '../../services';

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {

  public loading$;
  public messages$ = this.messageService.messages$;

  constructor(
    private modalCtrl: ModalController,
    private messageService: MessageService,
    private overlaysService: OverlaysService,
    private profileService: ProfileService) { }

  ionViewWillEnter() {
    this.getMessages();
  }

  private getMessages() {
    this.loading$ = this.overlaysService.buildLoader(this.messageService.getMessages(this.profileService.currentProfile._id));
  }

  public openMessageDetail(message: Message) {

    const messageDetailModal = this.modalCtrl.create(MessageDetailModalComponent, {
      message
    });

    messageDetailModal.present();
  }
}