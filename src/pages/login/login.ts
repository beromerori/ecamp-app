import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Rxjs
import { tap } from 'rxjs/operators';

// My models
import { UserCredentials } from '../../models';

// My pages
import { PreHomePage } from '../pre-home/pre-home';

// My services (barrel)
import { AuthService, OverlaysService, UserService } from '../../services';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public isRememberPassword: boolean = false;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private overlaysService: OverlaysService,
    private userService: UserService) { }

  public login(userCredentials: UserCredentials) {

    this.overlaysService.requestWithLoaderAndError(() => this.authService.login(userCredentials).pipe(
      tap(() => this.navCtrl.setRoot(PreHomePage))
    ));
  }

  public changePassword(newUserCredentials: UserCredentials) {
    this.overlaysService.requestWithLoaderAndError(() => this.userService.updateUserPassword(newUserCredentials), 'SERVER.MESSAGES.USER.NEW-PASSWORD');
  }
}