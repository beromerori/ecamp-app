import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// My pages
import { BillsPage } from '../bills/bills';
import { CampListPage } from '../programmings/camp-list/camp-list';
import { ChildsPage } from '../childs/childs';
import { MessagesPage } from '../messages/messages';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public pages: { id: string, title: string, icon: string, component: any }[] = [
    { id: 'profile', title: 'MENU.PROFILE', icon: 'fas fa-user-circle', component: ProfilePage },
    { id: 'applications', title: 'MENU.APPLICATIONS', icon: 'fas fa-file-invoice', component: BillsPage },
    { id: 'childs', title: 'MENU.INSTRUCTORS', icon: 'fas fa-child', component: ChildsPage },
    { id: 'messages', title: 'MENU.MESSAGES', icon: 'fas fa-envelope', component: MessagesPage },
    { id: 'programmings', title: 'MENU.PROGRAMMINGS', icon: 'fas fa-tasks', component: CampListPage }
  ];

  constructor(private navCtrl: NavController) { }

  public goToPage(page) {
    this.navCtrl.push(page.component);
  }
}