import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Rxjs
import { tap } from 'rxjs/operators';

// My pages
import { BillsPage } from '../bills/bills';
import { CampListPage } from '../programmings/camp-list/camp-list';
import { ChildsPage } from '../childs/childs';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { MessagesPage } from '../messages/messages';
import { ProfilePage } from '../profile/profile';

// My services (barrel)
import { AuthService } from '../../services/auth.service';
import { OverlaysService, ProfileService, UserService } from '../../services';

/**
 * Generated class for the PreHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pre-home',
  templateUrl: 'pre-home.html',
})
export class PreHomePage {

  public pages: { id: string, title: string, icon: string, component: any }[] = [];
  public rootPage: any = HomePage;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private overlaysService: OverlaysService,
    private profileService: ProfileService,
    private userService: UserService
  ) {

    this.pages = [
      { id: 'profile', title: 'MENU.PROFILE', icon: 'fas fa-user-circle', component: ProfilePage },
      { id: 'applications', title: 'MENU.APPLICATIONS', icon: 'fas fa-file-invoice', component: BillsPage },
      { id: 'childs', title: 'MENU.INSTRUCTORS', icon: 'fas fa-child', component: ChildsPage },
      { id: 'messages', title: 'MENU.MESSAGES', icon: 'fas fa-envelope', component: MessagesPage },
      { id: 'programmings', title: 'MENU.PROGRAMMINGS', icon: 'fas fa-tasks', component: CampListPage }
    ];
  }

  ionViewWillEnter() {
    this.getProfile();
  }

  private getProfile() {
    this.overlaysService.requestWithLoaderAndError(() => this.profileService.getProfile().pipe(
      tap(() => this.getChilds())
    ));
  }

  private getChilds() {
    this.overlaysService.requestWithLoaderAndError(() => this.userService.getChilds());
  }

  private pageNames = {
    'home': HomePage,
    'profile': ProfilePage,
    'applications': BillsPage,
    'childs': ChildsPage,
    'programmings': CampListPage,
    'messages': MessagesPage
  };

  public openPage(page) {
    this.navCtrl.push(this.pageNames[page.id]);
  }

  public logout() {
    this.authService.logout();
    this.navCtrl.setRoot(LoginPage);
  }
}