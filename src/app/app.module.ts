import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Plugins
import { CallNumber } from '@ionic-native/call-number';
import { Camera } from '@ionic-native/camera';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { EcampApp } from './app.component';

// My components
import { ComponentsModule } from '../components/components.module';

// My interceptors
import { RequestInterceptor } from '../interceptors/request.interceptor';
import { TokenInterceptor } from '../interceptors/token.interceptor';

// My pages
import { Pages } from '../pages';

// My services
import { Services } from '../services';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    EcampApp,
    ...Pages
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    IonicModule.forRoot(
      EcampApp,
      {
        backButtonText: '',
        mode: 'ios'
      }
    ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    EcampApp,
    ...Pages
  ],
  providers: [
    // Plugins
    CallNumber,
    Camera,
    EmailComposer,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    // Interceptors
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    // Services
    ...Services
  ]
})
export class AppModule { }