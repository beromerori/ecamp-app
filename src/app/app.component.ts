import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

// Plugins
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Translate
import { TranslateService } from '@ngx-translate/core';

// My pages
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class EcampApp {

  public rootPage: any = LoginPage;

  private lang: string = localStorage.getItem('lang') || 'es';

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private translateService: TranslateService) {

    this.translateService.setDefaultLang(this.lang);
    localStorage.setItem('lang', this.lang);

    this.initializeApp();
  }

  private initializeApp() {

    Promise.all([this.platform.ready(), this.translateService.use(this.lang).toPromise()])
      .then(() => {

        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.statusBar.styleLightContent();
        this.statusBar.overlaysWebView(false);
        //this.statusBar.backgroundColorByHexString('#171717');
      })
      .catch((error) => console.error('Error initializating app', error));

    /*this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.translateService.setDefaultLang('en');
    });*/
  }
}