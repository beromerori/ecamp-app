export const CONST = {

    ALERT_TYPES: {
        CONFIRM: 1,
        INFO: 2,
        ERROR: 3
    },

    APP_NAME: 'E-Camp',

    AUTONOMOUS_COMMUNITIES: [
        {
            id: 0,
            name: 'Andalucía',
            provinces: ['Almería', 'Cádiz', 'Córdoba', 'Granada', 'Huelva',
                'Jaén', 'Málaga', 'Sevilla']
        },
        {
            id: 1,
            name: 'Aragón',
            provinces: ['Huesca', 'Teruel', 'Zaragoza']
        },
        {
            id: 2,
            name: 'Asturias',
            provinces: ['Oviedo']
        },
        {
            id: 3,
            name: 'Baleares',
            provinces: ['Palma de Mallorca']
        },
        {
            id: 4,
            name: 'Canarias',
            provinces: ['Santa Cruz de Tenerife', 'Las Palmas de Gran Canaria']
        },
        {
            id: 5,
            name: 'Cantabria',
            provinces: ['Santander']
        },
        {
            id: 6,
            name: 'Castilla - La Mancha',
            provinces: ['Albacete', 'Ciudad Real', 'Cuenca', 'Guadalajara', 'Toledo']
        },
        {
            id: 7,
            name: 'Castilla y León',
            provinces: ['Ávila', 'Burgos', 'León', 'Salamanca', 'Segovia',
                'Soria', 'Valladolid', 'Zamora']
        },
        {
            id: 8,
            name: 'Cataluña',
            provinces: ['Barcelona', 'Gerona', 'Lérida', 'Tarragona']
        },
        {
            id: 9,
            name: 'Comunidad Valenciana',
            provinces: ['Alicante', 'Castellón', 'Valencia']
        },
        {
            id: 10,
            name: 'Extremadura',
            provinces: ['Badajoz', 'Cáceres']
        },
        {
            id: 11,
            name: 'Galicia',
            provinces: ['A-Coruña', 'Lugo', 'Orense', 'Pontevedra']
        },
        {
            id: 12,
            name: 'La Rioja',
            provinces: ['Logroño']
        },
        {
            id: 13,
            name: 'Madrid',
            provinces: ['Madrid']
        },
        {
            id: 14,
            name: 'Murcia',
            provinces: ['Murcia']
        },
        {
            id: 15,
            name: 'Navarra',
            provinces: ['Pamplona']
        },
        {
            id: 16,
            name: 'País Vasco',
            provinces: ['Bilbao', 'Guipúzcoa', 'San Sebastián', 'Vitoria']
        }
    ],

    CAMP_TYPES: {
        AUTONOMOUS: 1,
        EXCHANGE: 2
    },

    LANGUAGES: [
        {
            id: 0,
            name: 'LANGUAGES.SPANISH',
            code: 'es',
            icon: ''
        },
        {
            id: 1,
            name: 'LANGUAGES.ENGLISH',
            code: 'en',
            icon: ''
        }
    ],

    MODALITIES: [
        {
            id: 0,
            name: 'CAMPS.MODALITIES.CIRCUS',
            checked: false
        },
        {
            id: 1,
            name: 'CAMPS.MODALITIES.CLIMBING',
            checked: false
        },
        {
            id: 2,
            name: 'CAMPS.MODALITIES.CREATIVE',
            checked: false
        },
        {
            id: 3,
            name: 'CAMPS.MODALITIES.CULTURAL',
            checked: false
        },
        {
            id: 4,
            name: 'CAMPS.MODALITIES.ENGLISH',
            checked: false
        },
        {
            id: 5,
            name: 'CAMPS.MODALITIES.ENVIRONMENTAL',
            checked: false
        },
        {
            id: 6,
            name: 'CAMPS.MODALITIES.FRESH-AIR',
            checked: false
        },
        {
            id: 7,
            name: 'CAMPS.MODALITIES.GAMES',
            checked: false
        },
        {
            id: 8,
            name: 'CAMPS.MODALITIES.MULTI-ADVENTURE',
            checked: false
        },
        {
            id: 9,
            name: 'CAMPS.MODALITIES.MUSICAL',
            checked: false
        },
        {
            id: 10,
            name: 'CAMPS.MODALITIES.NAUTICAL',
            checked: false
        },
        {
            id: 11,
            name: 'CAMPS.MODALITIES.PHOTOGRAPHY',
            checked: false
        },
        {
            id: 12,
            name: 'CAMPS.MODALITIES.SCIENTIFIC',
            checked: false
        },
        {
            id: 13,
            name: 'CAMPS.MODALITIES.SPORTS-PRACTICES',
            checked: false
        },
        {
            id: 14,
            name: 'CAMPS.MODALITIES.THEATER',
            checked: false
        },
        {
            id: 15,
            name: 'CAMPS.MODALITIES.TREKKING',
            checked: false
        }
    ],

    PAYPAL: {
        ENV: ['sandbox', 'live'],
        CURRENCY: 'EUR'
    },

    ROLES: {
        ADMIN: 3,
        INSTRUCTOR: 2,
        PARENT: 1,
        CHILD: null
    },

    VALIDATIONS: {
        CREDIT_CARD_REGEX: /^((67\d{2})|(4\d{3})|(5[1-5]\d{2})|(6011))(-?\s?\d{4}){3}|(3[4,7])\ d{2}-?\s?\d{6}-?\s?\d{5}$/,
        DNI_REGEX: /^[0-9]{8,8}[A-Za-z]$/g,
        EMAIL_REGEX: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/,
        NIE_REGEX: /^[XYZ][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/i,
        NIF_REGEX: /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i,
        PASS_REGEX: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        PHONE_REGEX: /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/,
        PASSPORT_REGEX: /^[a-z]{3}[0-9]{6}[a-z]?$/i,
        POSTAL_CODE_REGEX: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/,
        URL_REGEX: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/
    }
};