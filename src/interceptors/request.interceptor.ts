import { Injectable } from '@angular/core';

import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

// ENV
import { environment } from '../environments/environment.dev';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    const includeAssets = request.url.includes('assets');

    if (!includeAssets) {
      request = request.clone({
        url: `${environment.ip}/${request.url}`
      });
    }

    return next.handle(request);
  }
}