export const environment = {
    production: true,
    mode: 'Production',
    ip: 'https://ecamp-back.herokuapp.com/ecamp/api'
};