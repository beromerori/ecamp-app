export const environment = {
    production: false,
    mode: 'Development',
    ip: 'https://ecamp-back.herokuapp.com/ecamp/api'
};