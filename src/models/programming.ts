import { MediaFile } from "./mediaFile";

export class Programming {

    _id?: string;
    title?: string;
    date?: {
        timestamp?: number;
        time?: string;
    };
    body?: string;
    images?: MediaFile[];
    camp_id?: string;
    selected?: boolean;

    constructor(data) {
        Object.assign(this, data);
    }

    static findById(programmings: Programming[], id: string) {
        return programmings.find(programming => programming._id === id);
    }

    static filterByCamp(programmings: Programming[], camp_id: string) {
        return programmings.filter(programming => programming.camp_id === camp_id);
    }
}