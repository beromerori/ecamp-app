import { User } from './user';
import { Parent } from './parent';

export class Child extends User {

    parent_id: string;
    intolerances: string;
    camp?: {
        camp_id?: string;
        campType: number;
        code: string;
        name: string;
    };

    constructor(data) {
        super(data);
    }

    static filterByParent(users, parent: Parent) {
        return users.filter(user => user.parent_id === parent._id);
    }

    static filterByCamp(users, camp_id: string) {
        return users.filter(user => user.camp && (user.camp.camp_id === camp_id));
    }
}