// My models
import { MediaFile } from "./mediaFile";

export class Camp {

    _id?: string;
    camp_id?: string;
    type?: number;
    name?: string;
    description?: string;
    images?: MediaFile[];
    pdf?: MediaFile;
    code?: string;
    ccaa?: number;
    province?: string;
    cp?: number;
    address?: string;
    installations?: string;
    date?: CampDate;
    status?: number;
    numDays?: number;
    numPeople?: number;
    numApplications?: number;
    age?: CampAge;
    price?: number;
    modalities?: any[];
    observations?: string;

    constructor(data) {
        Object.assign(this, data);
    }

    static findById(camps: Camp[], id: string) {
        return camps.find(camp => camp._id === id);
    }

    static filterByType(camps: Camp[], type: number) {
        return camps.filter(camp => camp.type === type);
    }
}

export interface CampAge {

    min?: number;
    max?: number;
}

export interface CampDate {

    start?: {
        timestamp?: number;
        time?: string;
    },
    end?: {
        timestamp?: number;
        time?: string;
    }
}

export interface CampFilters {

    active?: boolean;
    type?: number;
    name?: string;
    ccaa?: number;
    province?: string;
    numDays?: number;
    startDate?: number;
    endDate?: number;
    status?: number;
    minAge?: number;
    maxAge?: number;
    price?: number;
    modalities?: string;
    first_id?: string;
    last_id?: string;
    page?: number;
    limit?: number;
}