export class Chart {

    totalsAndByType: {
        totalCamps: number,
        totalApplications: number,
        campsByType: {
            labels: string[],
            numCamps: number[],
            numApplications: number[]
        }
    };

    byModalities: {
        labels: string[],
        data: number[]
    };
}