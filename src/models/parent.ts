import { User } from './user';

export class Parent extends User {

    childs_ids: string[];
    bonus?: string;

    constructor(data) {
        super(data);
    }
}