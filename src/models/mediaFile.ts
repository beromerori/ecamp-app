export interface MediaFile {

    alt?: string,
    bytes?: number,
    created_at?: string,
    etag?: string,
    format?: string,
    height?: number,
    index?: number,
    placeholder?: boolean,
    public_id?: string,
    resource_type?: string,
    secure_url?: string,
    signature?: string,
    tags?: [string],
    type?: string,
    url?: string,
    version?: number,
    width?: number
}

export interface MediaFileDeleted {
    file: string;
}