export class Bill {

    _id?: string;
    camps?: CampBill[];
    user?: UserBill;
    total?: number;

    collapse?: boolean;

    constructor(data) {
        Object.assign(this, data);
        this.collapse = true;
    }
}

export interface CampBill {

    _id?: string;
    campType?: number;
    code?: string;
    name?: string;
    price?: number;
}

export interface UserBill {

    dni?: string;
    email?: string;
    name?: string;
    phone?: string;
    surnames?: string;
}