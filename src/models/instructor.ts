import { User } from './user';
import { MediaFile } from './mediaFile';

export class Instructor extends User {

    camp?: {
        camp_id?: string;
        campType: number;
        code: string;
        name: string;
    };
    cv?: MediaFile;

    constructor(data) {
        super(data);
    }
}