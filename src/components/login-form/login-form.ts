import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// CONSTANTS
import { CONST } from '../../app/app.constants';

// My models
import { UserCredentials } from '../../models';

/**
 * Generated class for the LoginFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-login-form',
  templateUrl: 'login-form.html'
})
export class LoginFormComponent implements OnInit {

  @Input() public isRememberPassword: boolean = false;

  @Output('login') public login: EventEmitter<UserCredentials> = new EventEmitter<UserCredentials>();
  @Output('changePassword') public changePassword: EventEmitter<UserCredentials> = new EventEmitter<UserCredentials>();

  public loginForm: FormGroup;
  public changePasswordForm: FormGroup;

  public hide: boolean = true;
  public iconVisivility: string = 'fas fa-eye';

  public passwordsInvalid: boolean = false;

  constructor() { }

  ngOnInit() {
    this.initLoginForm();
    this.initRememberPasswordForm();
  }

  private initLoginForm() {

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      pass: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)])
    });
  }

  get loginEmail() { return this.loginForm.get('email'); }
  get pass() { return this.loginForm.get('pass'); }

  private initRememberPasswordForm() {

    this.changePasswordForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.EMAIL_REGEX)]),
      newPassword: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)]),
      repeatPassword: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)]),
    });

    this.repeatPassword.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.newPassword.value)
    );
  }

  get email() { return this.changePasswordForm.get('email'); }
  get newPassword() { return this.changePasswordForm.get('newPassword'); }
  get repeatPassword() { return this.changePasswordForm.get('repeatPassword'); }

  public getPasswordType() {
    return this.hide ? 'password' : 'text';
  }

  public changeVisibilityPassword() {
    this.hide = !this.hide;
    this.iconVisivility = this.hide ? 'fas fa-eye' : 'fas fa-eye-slash';
  }

  public showChangePassword() {
    this.isRememberPassword = true;
    this.changePasswordForm.reset();
  }

  public send() {
    this.login.emit(this.loginForm.value);
  }

  public sendNewPassword() {
    this.isRememberPassword = false;
    this.changePassword.emit({ email: this.email.value, pass: this.newPassword.value });
  }

  public cancel() {
    this.isRememberPassword = false;
    this.loginForm.reset();
  }
}