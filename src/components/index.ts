// Empty-state
import { EmptyStateComponent } from './empty-state/empty-state';
export { EmptyStateComponent } from './empty-state/empty-state';

// Input-error
import { InputErrorComponent } from './input-error/input-error';
export { InputErrorComponent } from './input-error/input-error';

// Loading
import { LoadingComponent } from './loading/loading';
export { LoadingComponent } from './loading/loading';

// Login-form
import { LoginFormComponent } from './login-form/login-form';
export { LoginFormComponent } from './login-form/login-form';

// Modal-toolbar
import { ModalToolbarComponent } from './modal-toolbar/modal-toolbar';
export { ModalToolbarComponent } from './modal-toolbar/modal-toolbar';

// Modality-badge
import { ModalityBadgeComponent } from './modality-badge/modality-badge';
export { ModalityBadgeComponent } from './modality-badge/modality-badge';

export const Components: any = [
    EmptyStateComponent,
    InputErrorComponent,
    LoadingComponent,
    LoginFormComponent,
    ModalToolbarComponent,
    ModalityBadgeComponent
];