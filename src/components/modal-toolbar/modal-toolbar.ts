import { Component, Input } from '@angular/core';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalToolbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-modal-toolbar',
  templateUrl: 'modal-toolbar.html'
})
export class ModalToolbarComponent {

  @Input() title: string;

  constructor(private viewCtrl: ViewController) { }

  public dismiss() {
    this.viewCtrl.dismiss();
  }
}