import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';

// Plugins
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

// Rxjs
import { tap } from 'rxjs/operators';

// My models
import { Instructor } from '../../../models';

// My services (barrel)
import { OverlaysService, UserService } from '../../../services';

/**
 * Generated class for the InstructorDetailModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'instructor-detail-modal',
  templateUrl: 'instructor-detail-modal.html'
})
export class InstructorDetailModalComponent implements OnInit {

  public loading$;
  public instructor$;

  constructor(
    private callNumber: CallNumber,
    private emailComposer: EmailComposer,
    private iab: InAppBrowser,
    private navParams: NavParams,
    private overlaysService: OverlaysService,
    private userService: UserService) { }

  ngOnInit() {

    const camp_id: string = this.navParams.get('camp_id');
    this.getInstructor(camp_id);
  }

  private getInstructor(camp_id: string) {

    this.loading$ = this.overlaysService.buildLoader(this.userService.getInstructor(camp_id).pipe(
      tap((users: any[]) => {

        const instructor = users.find(user => user.role && user.camp && (user.camp.camp_id === camp_id));
        if (instructor) this.instructor$ = this.userService.selectById(instructor._id);
      })
    ));
  }

  public openEmail(instructor: Instructor) {

    const email = {
      to: instructor.email,
      isHtml: true
    };

    this.emailComposer.open(email);
  }

  public openPhone(instructor: Instructor) {
    this.callNumber.callNumber(instructor.phone.toString(), false);
  }

  public openCV(instructor: Instructor) {
    const url = instructor.cv.secure_url || instructor.cv.url;
    this.iab.create(url, '_system');
  }
}