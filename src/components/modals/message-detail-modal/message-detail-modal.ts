import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

// My models
import { Message } from '../../../models';

/**
 * Generated class for the MessageDetailModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'message-detail-modal',
  templateUrl: 'message-detail-modal.html'
})
export class MessageDetailModalComponent {

  public message: Message;

  constructor(private navParams: NavParams) {

    this.message = this.navParams.get('message');
  }
}