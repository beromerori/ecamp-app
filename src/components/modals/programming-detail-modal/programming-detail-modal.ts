import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

// My models
import { Programming } from '../../../models';

/**
 * Generated class for the ProgrammingDetailModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'programming-detail-modal',
  templateUrl: 'programming-detail-modal.html'
})
export class ProgrammingDetailModalComponent {

  public programming: Programming;

  constructor(private navParams: NavParams) {

    this.programming = this.navParams.get('programming');
  }
}