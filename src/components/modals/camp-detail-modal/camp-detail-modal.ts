import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';

// Plugins
import { InAppBrowser } from '@ionic-native/in-app-browser';

// My models
import { Camp } from '../../../models';

// My Services (barrel)
import { CampService, OverlaysService } from '../../../services';

/**
 * Generated class for the CampDetailModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'camp-detail-modal',
  templateUrl: 'camp-detail-modal.html'
})
export class CampDetailModalComponent implements OnInit {

  private camp: Camp;

  public loading$;
  public camp$;

  constructor(
    private iab: InAppBrowser,
    private navParams: NavParams,
    private campService: CampService,
    private overlaysService: OverlaysService) {

    this.camp = this.navParams.get('camp');
  }

  ngOnInit() {
    this.getCamp();
  }

  private getCamp() {
    this.loading$ = this.overlaysService.buildLoader(this.campService.getCamp(this.camp));
    this.camp$ = this.campService.selectById(this.camp.camp_id);
  }

  public openPDF(camp: Camp) {
    const url = camp.pdf.secure_url || camp.pdf.url;
    this.iab.create(url, '_system');
  }
}