// Camp-detail
import { CampDetailModalComponent } from './camp-detail-modal/camp-detail-modal';
export { CampDetailModalComponent } from './camp-detail-modal/camp-detail-modal';

// Instructor-detail
import { InstructorDetailModalComponent } from './instructor-detail-modal/instructor-detail-modal';
export { InstructorDetailModalComponent } from './instructor-detail-modal/instructor-detail-modal';

// Message-detail
import { MessageDetailModalComponent } from './message-detail-modal/message-detail-modal';
export { MessageDetailModalComponent } from './message-detail-modal/message-detail-modal';

// ProgrammingDetailModalComponent
import { ProgrammingDetailModalComponent } from './programming-detail-modal/programming-detail-modal';
export { ProgrammingDetailModalComponent } from './programming-detail-modal/programming-detail-modal';

export const Modals: any[] = [
    CampDetailModalComponent,
    InstructorDetailModalComponent,
    MessageDetailModalComponent,
    ProgrammingDetailModalComponent
];