import { Component, Input } from '@angular/core';

/**
 * Generated class for the ModalityBadgeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-modality-badge',
  templateUrl: 'modality-badge.html'
})
export class ModalityBadgeComponent {

  @Input() id: number;
  @Input() name: string;
  @Input() checked: boolean;

  public type: string;

  constructor() { }

  ngOnInit() {
    this.type = `modality-${this.id}`;
  }
}