import { Component, Input } from '@angular/core';

/**
 * Generated class for the InputErrorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-input-error',
  templateUrl: 'input-error.html'
})
export class InputErrorComponent {

  @Input() public control;
  @Input() public message;

  constructor() { }
}