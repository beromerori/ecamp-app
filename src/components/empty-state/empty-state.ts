import { Component, Input } from '@angular/core';

/**
 * Generated class for the EmptyStateComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-empty-state',
  templateUrl: 'empty-state.html'
})
export class EmptyStateComponent {

  @Input() public message: string = '';

  constructor() { }
}