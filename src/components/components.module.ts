import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';

// Translate
import { TranslateModule } from '@ngx-translate/core';

// My components
import { Components } from './index';

// My modals
import { Modals } from './modals';

// My pipes
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
	declarations: [
		// Components
		...Components,
		// Modals
		...Modals
	],
	imports: [
		IonicModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		PipesModule
	],
	entryComponents: [
		...Modals
	],
	exports: [
		...Components
	]
})
export class ComponentsModule { }